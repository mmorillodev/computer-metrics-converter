import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public selectedMetricFrom: string;
  public selectedMetricTo: string;

  public userInput: number;
  public result: number;

  public metrics = {
    bit: 1,
    byte: 8,
    kilobit: 1e3,
    kilobyte: 8e3,
    megabit: 1e6,
    megabyte: 8e6,
    gigabit: 1e9,
    gigabyte: 8e9,
    terabit: 1e12,
    terabyte: 8e12
  };

  constructor() {}

  public invert() {
    let aux: any = this.selectedMetricFrom;

    this.selectedMetricFrom = this.selectedMetricTo;
    this.selectedMetricTo = aux;

    aux = this.userInput;
    this.userInput = this.result;
    this.result = aux;
  }

  public capitalize(value: string): string {
    if (value) {
      return value.charAt(0).toUpperCase() + value.slice(1);
    }

    return '';
  }

  public inputChangeListener() {
    if (this.userInput && this.selectedMetricFrom && this.selectedMetricTo) {
      this.apply();
    }
  }

  private apply() {

  }
}
